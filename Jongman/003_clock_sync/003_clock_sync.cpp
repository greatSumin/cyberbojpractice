#include<cstdio>
#include<cstring>
#include<algorithm>
namespace sumin {
int C;
int time[16];
bool swtch[10][16];
const int INF = 987654321;

void init() {
	memset(swtch, 0, sizeof(swtch));
	swtch[0][0] = swtch[0][1] = swtch[0][2] = true;
	swtch[1][3] = swtch[1][7] = swtch[1][9] = swtch[1][11] = true;
	swtch[2][4] = swtch[2][10] = swtch[2][14] = swtch[2][15] = true;
	swtch[3][0] = swtch[3][4] = swtch[3][5] = swtch[3][6] = swtch[3][7] = true;
	swtch[4][6] = swtch[4][7] = swtch[4][8] = swtch[4][10] = swtch[4][12] = true;
	swtch[5][0] = swtch[5][2] = swtch[5][14] = swtch[5][15] = true;
	swtch[6][3] = swtch[6][14] = swtch[6][15] = true;
	swtch[7][4] = swtch[7][5] = swtch[7][7] = swtch[7][14] = swtch[7][15] = true;
	swtch[8][1] = swtch[8][2] = swtch[8][3] = swtch[8][4] = swtch[8][5] = true;
	swtch[9][3] = swtch[9][4] = swtch[9][5] = swtch[9][9] = swtch[9][13] = true;
}

void click_swtch(int sw) {
	for(int i = 0;i<16;i++)
		if(swtch[sw][i])
			time[i] = (time[i]+1)%4;
}

int go(int num) {
	if(num==10) {
		for(int i = 0;i<16;i++)
			if(time[i])
				return INF+1;
		return 0;
	}
	int ret = INF;
	for(int click=0;click<4;click++) {
		ret = std::min(ret, click + go(num+1));
		click_swtch(num);
	}
	return ret;
}

int main() {
	init();
	scanf("%d", &C);
	int res;
	while(C--) {
		memset(time, 0, sizeof(time));
		for(int i = 0;i<16;i++) {
			scanf("%d", &time[i]);
			time[i] = (time[i]/3)%4;
		}
		res = go(0);
		if(res>=INF)
			res = -1;
		printf("%d\n", res);
	}
}
}
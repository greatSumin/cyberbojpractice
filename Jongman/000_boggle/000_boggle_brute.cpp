#include<iostream>
#include<string>

using namespace std;

bool** result;
char boggle[5][5];
int dx[] = {-1, 0, 1, -1, 1, -1, 0, 1};
int dy[] = {-1, -1, -1, 0, 0, 1, 1, 1};

bool isInBound(int x) {
	if(x>=0 && x<5)
		return true;
	return false;
}

bool hasWord(int row, int col, string word) {
	if(word.length()==0)
		return true;
	// base case
	
	if(boggle[row][col]==word[0])
		for(int direction = 0; direction < 8 ; direction++) {
			int to_x = row+dx[direction];
			int to_y = col+dy[direction];
			if(isInBound(to_x)&&isInBound(to_y))
				if(hasWord(to_x, to_y, word.substr(1)))
					return true;
		}
	
	return false;
}

string yesOrno(bool x) {
	if(x)
		return "YES";
	else
		return "NO";
}

int main (void) {
	int test_case;
	cin >> test_case;
	
	result = new bool*[test_case];
	string** word = new string*[test_case];
	
	for(int t = 0;t < test_case;t++) {
		
		for(int row = 0; row < 5 ;row++)
			for(int col = 0 ; col < 5 ; col++)
				cin >> boggle[row][col];
		
		int word_num;
		cin >> word_num;
		
		result[t] = new bool[10];
		word[t] = new string[10];
		
		for(int w = 0 ; w < word_num ; w++) {
			cin >> word[t][w];
			
			for(int row = 0; row < 5 ;row++)
				for(int col = 0 ; col < 5 ; col++)
					result[t][w] = result[t][w] || hasWord(row, col, word[t][w]);
		}
	}
	
	for(int t = 0;t<test_case;t++) {
		for(int w = 0;w<10;w++) {
			if(word[t][w].length() != 0) {
				cout << word[t][w] << " " << yesOrno(result[t][w]) << endl;
			}
		}
	}
}
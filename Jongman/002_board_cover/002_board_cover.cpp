#include<cstdio>
#include<cstring>

int C, H, W;
char str[20];
int board[20][20];
int block[4][3][2] = {
	{
		{0, 0},
		{1, 0},
		{1, -1}
	},
	{
		{0, 0},
		{0, 1},
		{1, 0}
	},
	{
		{0, 0},
		{1, 0},
		{1, 1}
	},
	{
		{0, 0},
		{0, 1},
		{1, 1}
	}	
};

bool set(int y, int x, int type, int plus) {
	bool ret = true;
	for(int tile=0;tile<3;tile++) {
		int ty = y + block[type][tile][0];
		int tx = x + block[type][tile][1];
		if(ty<0||ty>=H||tx<0||tx>=W) { // 범위를 벗어났으면
			ret = false;
			continue;
		}
		if((board[ty][tx] += plus)==2)
			ret = false;
	}
	return ret;
}

int go() {
	// base case
	int h = -1, w = -1;
	for(int i = 0;i<H;i++) {
		for(int j = 0;j<W;j++)
			if(!board[i][j]) {
				h = i; w = j;
				break;
			}
		if(h!=-1)
			break;
	}
	if(h==-1) return 1;
	int ret = 0;
	for(int t = 0; t < 4; ++t) {
		if(set(h, w, t, 1))
			ret += go();
		set(h, w, t, -1);
	}
	return ret;
}

int main (void) {
	scanf("%d", &C);
	while(C--) {
		scanf("%d %d", &H, &W);
		memset(board, 0, sizeof(board));
		for(int h = 0; h < H; ++h) {
			scanf("%s", str);
			for(int w = 0; w < W; ++w)
				if(str[w]=='#')
					board[h][w] = 1;
		}
		printf("%d\n", go());
	}
}
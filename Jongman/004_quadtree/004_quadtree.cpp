#include<iostream>
#include<string>

using namespace std;

int C;
string input;

string reverse(string::iterator& iter) {
	char head = *iter;
	++iter;
	if(head=='w'||head=='b')
		return string(1, head);
	
	string upperLeft = reverse(iter);
	string upperRight = reverse(iter);
	string lowerLeft = reverse(iter);
	string lowerRight = reverse(iter);
	return string("x") + lowerLeft + lowerRight + upperLeft + upperRight;
}

int main() {
	cin >> C;
	while(C--) {
		cin >> input;
		string::iterator iter = input.begin();
		cout << reverse(iter) << endl;
	}
}
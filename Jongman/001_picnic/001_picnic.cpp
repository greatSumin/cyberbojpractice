#include<iostream>
#include<cstring>

using namespace std;

bool** Pair;
int* result;
int stu_num, pair_num;

int pairing(bool* taken) {
	int firstfree = 0;
	for(;firstfree < stu_num ; ++firstfree)
		if(!taken[firstfree])
			break;
	
	if(firstfree == stu_num)
		return 1;
	// base case
	
	int ret = 0;
	for(int pairwith = firstfree + 1 ; pairwith < stu_num ; ++pairwith) {
		if(Pair[firstfree][pairwith]&&!taken[pairwith]) {
			taken[firstfree] = taken[pairwith] = true;
			ret += pairing(taken);
			taken[firstfree] = taken[pairwith] = false;
		}
	}
	
	return ret;
}

int main(void) {
	int test_case;
	cin >> test_case;
	result = new int[test_case];
	
	for(int t = 0 ; t < test_case ; ++t) {
		cin >> stu_num >> pair_num;
		
		Pair = new bool*[stu_num];
		
		for(int i = 0; i < stu_num; ++i)
			Pair[i] = new bool[stu_num];
		
		for(int i = 0;i<stu_num;++i)
			for(int j = 0 ; j<stu_num;++j)
				Pair[i][j] = false;
		// 더 우아하게 할 수 있는 방법은 없을까?
		
		int x, y;
		for(int i = 0; i < pair_num; ++i) {
			cin >> x >> y;
			if(x>y) {
				int t = x;
				x = y;
				y = t;
			}
			
			Pair[x][y] = true;
		}
		// 입력 완료
		
		bool* taken = new bool[stu_num];
		for(int i = 0;i<stu_num;i++)
			taken[i] = false;
		result[t] = pairing(taken);
	}
	
	for(int t = 0;t<test_case;t++)
		cout << result[t] << endl;
}
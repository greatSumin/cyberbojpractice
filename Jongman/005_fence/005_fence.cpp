#include<cstdio>
#include<cstring>
#include<algorithm>

using namespace std;

int C, N;
int fence[20000];

int go(int left, int right) {
	printf("Here! : %d %d\n", left, right);
	if(left>right)
		return 0;
	if(left==right)
		return fence[left];
	
	int mid = (right + left) / 2;
	int ret = max(go(left, mid), go(mid+1, right));
	
	int lo = mid, hi = mid+1;
	int height = min(fence[lo], fence[hi]);
	ret = max(ret, height*2);
	while(left<lo||hi<right) {
		if(hi<right&&(lo==left||fence[lo-1]<fence[hi+1])) {
			++hi;
			height = min(height, fence[hi]);
		}
		else {
			--lo;
			height = min(height, fence[lo]);
		}
		ret = max(ret, height*(hi-lo+1));
	}
	return ret;
}

int main() {
	scanf("%d", &C);
	while(C--) {
		scanf("%d", &N);
		for(int i = 0;i<N;i++)
			scanf("%d", &fence[i]);
		printf("%d\n", go(0, N-1));
	}
}
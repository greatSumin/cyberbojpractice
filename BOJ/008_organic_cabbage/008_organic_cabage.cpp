#include<cstdio>
#include<cstring>

int T, M, N, K;
bool board[50][50];
bool visited[50][50];

void dfs(int i, int j) {
	if(visited[i][j])
		return;
	visited[i][j] = true;
	if(!board[i][j])
		return;
	if(i-1>=0)
		dfs(i-1, j);
	if(j-1>=0)
		dfs(i, j-1);
	if(i+1<N)
		dfs(i+1, j);
	if(j+1<M)
		dfs(i, j+1);
}

int main() {
	int X,Y;
	
	scanf("%d", &T);
	while(T--) {
		scanf("%d %d %d", &N, &M, &K);
		memset(board, 0, sizeof(board));
		memset(visited, 0, sizeof(visited));
		while(K--) {
			scanf("%d %d", &X, &Y);
			board[X][Y] = true;
		}
		K++;
		for(int i = 0;i<N;i++)
			for(int j = 0;j<M;j++) {
				if(board[i][j]&&!visited[i][j]) {
					dfs(i, j);
					K++;
				}
			}
		printf("%d\n", K);
	}
}
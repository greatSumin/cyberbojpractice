#include<cstdio>
#define go(m, a) (m*a%10)
int T, a, b;
int m, n;

int main() {
	scanf("%d", &T);
	while(T--) {
		scanf("%d %d", &a, &b);
        a %= 10;
		
		n = 1;
		m = a;
		
		while((m = go(m,a))!=a)
			++n;
		if(b%n)
			n = b%n;
		
		--n;
		
		while(n--)
			m = go(m,a);
		if(!m)
			m = 10;
		printf("%d\n", m);
	}
}
#include<cstdio>
#include<math.h>

int T, x, y;
int dist, num;

int main() {
	scanf("%d", &T);
	while(T--) {
		scanf("%d %d", &x, &y);
		dist = y - x; num = 0;
		double dist_sqrt = sqrt(dist);
		double temp = (int)(dist_sqrt + 0.5);
		if(dist_sqrt<=temp)
			num = -1;
		num += temp * 2;
		
		printf("%d\n", num);
	}
}
#include<iostream>
#include<queue>
#include<cstring>
#include<algorithm>

using namespace std;

int main(void) {
	int testCase;
	cin >> testCase;
	
	int* result = new int[testCase];
	
	for(int t = 0; t < testCase ; t++) {
		
		int build_num, rule_num;
		
		cin >> build_num >> rule_num;
		
		int* build_time = new int[build_num];
		int* indegree = new int[build_num];
		int* memo = new int[build_num];
		bool** rule = new bool*[build_num];
		
		for(int i = 0; i < build_num ; ++i)
			rule[i] = new bool[build_num];
				
		//memset(indegree, 0, sizeof(indegree));
		/*
		ㄴ이걸 했을 때는 큰 값에서 오류가 났다!! 왜 그런지 찾아보자!
		*/
		//memset(rule, 0, sizeof(bool)*build_num*build_num);
		/*
			bool은 memset으로 초기화 못하는건지 찾아보자!
		*/
		for(int i = 0;i<build_num;i++)
			for(int j = 0;j<build_num;j++)
				rule[i][j] = false;
		
		
		for(int i = 0; i < build_num ; ++i) {
			cin >> build_time[i];
			memo[i] = build_time[i];
			indegree[i] = 0;
		}
		
		for(int i = 0 ; i < rule_num ; ++i) {
			int x, y;
			cin >> x >> y;
			rule[x - 1][y - 1] = true;
			++indegree[y - 1];
		}
		
		int goal;
		cin >> goal;
		
		// 모든 입력 완료
		
		queue<int> q;
		
		for(int i = 0; i < build_num ; ++i)
			if(!indegree[i])
				q.push(i);
		
		while(!q.empty()) {
			int v = q.front();
			q.pop();
			for(int i = 0;i < build_num ;++i)
				if(rule[v][i]) {
					memo[i] = max(memo[i], memo[v] + build_time[i]);
					if(--indegree[i] == 0)
						q.push(i);
				}
		}
		
		result[t] = memo[goal - 1];
		delete[] memo;
		for(int i = 0 ; i < build_num ; ++i)
			delete[] rule[i];
		delete[] rule;
		delete[] build_time;
		delete[] indegree;
	}
	
	for(int t = 0 ; t < testCase ; ++t)
		cout << result[t] << endl;
	
	delete[] result;
	
	return 0;
}
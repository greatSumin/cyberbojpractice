#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>

using namespace std;

const double INF = 987654321;

int T, N;
int point[20][2];
int vx, vy;

double go(int x, int plus, int minus) {
	if(x==N)
		return sqrt(pow(vx,2)+pow(vy,2));
	
	double ret = INF;
	if(plus<N/2) {
		vx += point[x][0];
		vy += point[x][1];
		
		ret = min(ret, go(x+1, plus+1, minus));
		
		vx -= point[x][0];
		vy -= point[x][1];		
	}
	if(minus<N/2) {
		vx -= point[x][0];
		vy -= point[x][1];
		
		ret = min(ret, go(x+1, plus, minus+1));
		
		vx += point[x][0];
		vy += point[x][1];
	}
	
	return ret;
}

int main() {
	scanf("%d", &T);
	while(T--) {
		scanf("%d", &N);
		for(int i = 0;i<N;++i)
			scanf("%d %d", &point[i][0], &point[i][1]);
		vx = vy = 0;
		printf("%lf\n", go(0,0,0));
	}
}
#include<cstdio>
#include<cstring>

int T, N;
int memo[41][2];
int** result;

int main() {	
	memset(memo, 0, sizeof(memo));
	memo[0][0] = 1;
	memo[0][1] = 0;
	memo[1][0] = 0;
	memo[1][1] = 1;
	
	scanf("%d", &T);
	result = new int*[T];
	for(int i = 0;i<T;++i)
		result[i] = new int[2];
	
	for(int t = 0;t<T;++t) {
		scanf("%d", &N);
		for(int i = 2;i<=N;++i) {
			if(memo[i][0])
				continue;
			memo[i][0] = memo[i-2][0] + memo[i-1][0];
			memo[i][1] = memo[i-2][1] + memo[i-1][1];
		}
		result[t][0] = memo[N][0];
		result[t][1] = memo[N][1];
	}
	for(int t = 0;t<T;++t)
		printf("%d %d\n", result[t][0], result[t][1]);
	
	return 0;
}
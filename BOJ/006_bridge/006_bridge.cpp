#include<cstdio>
#include<cstring>

int T, N, M;
int memo[30][30];

int go() {
	for(int i = 0;i<=N;i++)
		for(int j = 0;j<=M;j++) {
			if(i==j||j==0)
				memo[i][j] = 1;
			else
				memo[i][j] = memo[i-1][j-1] + memo[i-1][j];
		}
	
	return memo[N][M];
}

int main() {
	scanf("%d", &T);
	while(T--) {
		memset(memo, 0, sizeof(memo));
		scanf("%d %d", &M, &N);
		printf("%d\n", go());
	}
}
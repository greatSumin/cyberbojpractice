using System;
using System.Text;
using System.Collections.Generic;
using static System.Console;

namespace SuminAlgo2
{
    class Program
    {
        static void Main(string[] Args)
        {
			acm_craft instance = new acm_craft();
        }
    }
	
	public class acm_craft {
		public acm_craft() {
			int test_case = int.Parse(Console.ReadLine());
			int[] result = new int[test_case];
			string[] temp;
			for(int t = 0;t<test_case;t++) {
				temp = Console.ReadLine().Split();
				int build_num = int.Parse(temp[0]);
				int rule_num = int.Parse(temp[1]);
				
				int[] memo = new int[build_num];
				int[] build_time = new int[build_num];
				bool[,] connection = new bool[build_num, build_num];
				int[] indegree = new int[build_num];
				
				temp = Console.ReadLine().Split();
				for(int i = 0;i<build_num;i++)
					build_time[i] = memo[i] = int.Parse(temp[i]);
				
				int from_num, to_num;
				for(int i = 0;i<rule_num;i++) {
					temp = Console.ReadLine().Split();
					from_num = int.Parse(temp[0]);
					to_num = int.Parse(temp[1]);
					connection[from_num - 1, to_num - 1] = true;
					indegree[to_num - 1]++;
				}
				int goal = int.Parse(Console.ReadLine());
				
				// testCase 입력완료
				
				Queue<int> q = new Queue<int>();
				for(int i = 0;i<build_num;i++) {
					if(indegree[i]==0)
						q.Enqueue(i);
				}
				
				while(q.Count != 0) {
					int v = q.Dequeue();
					for(int i = 0;i<build_num;i++)
						if(connection[v, i]) {
							memo[i] = Math.Max(memo[i], memo[v] + build_time[i]);
							if(--indegree[i]==0)
								q.Enqueue(i);
						}
				}
				
				result[t] = memo[goal - 1];
			}
			
			for(int t = 0;t<test_case;t++)
				Console.WriteLine(result[t]);
		}
	}
}
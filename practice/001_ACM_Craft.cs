using System;
using System.Text;
using System.Collections.Generic;
using static System.Console;

namespace SuminAlgo
{
    class Program
    {
        static void Main(string[] Args)
        {
			acm_craft instance = new acm_craft();
        }
    }
	
	public class acm_craft {
		private int[] memo;
		private bool[,] connection;
		private int[] build_time;
		
		private int solve(int goal, int build_num) {
			if(memo[goal] > 0)
				return memo[goal];
			
			int result = 0;
			for(int i = 0;i<build_num;i++)
				if(connection[i, goal])
					result = Math.Max(result, solve(i, build_num));
			memo[goal] = result + build_time[goal];
			return memo[goal];
		}
		
		public acm_craft() {
			int test_case = int.Parse(Console.ReadLine());
			int[] result = new int[test_case];
			string[] temp;
			for(int t = 0;t<test_case;t++) {
				temp = Console.ReadLine().Split();
				int build_num = int.Parse(temp[0]);
				int rule_num = int.Parse(temp[1]);
				
				memo = new int[build_num];
				build_time = new int[build_num];
				connection = new bool[build_num, build_num];
				
				temp = Console.ReadLine().Split();
				for(int i = 0;i<build_num;i++)
					build_time[i] = int.Parse(temp[i]);
				
				int from_num, to_num;
				for(int i = 0;i<rule_num;i++) {
					temp = Console.ReadLine().Split();
					from_num = int.Parse(temp[0]);
					to_num = int.Parse(temp[1]);
					connection[from_num - 1, to_num - 1] = true;
				}
				int goal = int.Parse(Console.ReadLine());
				
				// testCase 입력완료
				
				result[t] = solve(goal - 1, build_num);
			}
			for(int t = 0;t<test_case;t++)
				Console.WriteLine(result[t]);
		}
	}
}